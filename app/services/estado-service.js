'use strict';

app.factory('EstadoService', function() {
	let data = [
		{id:1, nombre:'Pendiete',orden:1},
		{id:2, nombre:'Mirando',orden:2},
		{id:3, nombre:'Vista',orden:3}
	];

	let cant = data.length;

  	return {
		getAll: function(){
			return new Promise(function(resolve, reject){
				resolve(data);
			});
		},

		getNext: function(estado){
			let next = (estado.orden + 1) % (cant + 1);
			return get(data, next, NotNextException);
		},

		getPrev: function(estado){
			let prev = (estado.orden - 1) % (cant + 1);
			return get(data, prev, NotPrevException);
		}

  	};
});

function NotNextException(){}
function NotPrevException(){}

function get(data, nextORprev, exception){
	if(nextORprev === 0){
		throw new exception();
	}
	return data.find(function(estado){
		return estado.orden === nextORprev;
	});
}