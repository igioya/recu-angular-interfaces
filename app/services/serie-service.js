'use strict';

app.factory('SerieService', function() {
	let data = [
		{id:1, nombre:'Breaking Bad', temporadas:6, estado:{id:1, nombre:'Pendiete',orden:1}},
		{id:2, nombre:'Black Mirrors', temporadas:7, estado:{id:3, nombre:'Vista',orden:3}},
		{id:3, nombre:'Lost', temporadas:10, estado:{id:2, nombre:'Mirando', orden:2}},
		{id:4, nombre:'Vikingos', temporadas:7, estado:{id:3, nombre:'Vista',orden:3}},
		{id:5, nombre:'Stranger Things 2', temporadas:5, estado:{id:1, nombre:'Pendiete',orden:1}},
		{id:6, nombre:'Stranger Things', temporadas:2, estado:{id:3, nombre:'Vista',orden:3}}
	];

  	return {
		getAll: function(){

			return new Promise(function(resolve, reject){
				resolve(data);
			});
		},

		updateSerie: function(newSerie){
			angular.forEach(data, function(serie) {
				if(serie.id === newSerie.id){
					serie.estado = newSerie.estado;
				}
			});

			console.log(data);
		}

  	};
});