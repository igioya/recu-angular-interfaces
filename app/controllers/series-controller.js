'use strict';

app.controller('SeriesCtrl', function($location, SerieService, EstadoService, series) {
	let self = this;

	self.series = series;
	self.serie = null;
	self.error = '';

	self.editar = function(serie){
		self.serie = serie;
	}

	self.avanzarEstado = function(){
		self.setError('');
		get(EstadoService.getNext, "Ya no puedes avanzar el estado");
	}

	self.retrocederEstado = function(){
		self.setError('');
		get(EstadoService.getPrev, "Ya no puedes retroceder el estado");
	}

	let get = function(func, msgError){
		try{
			self.serie.estado = func(self.serie.estado);			
		} catch(e){
			self.setError(msgError);
		}
	}

	self.setError = function(content){
		self.error = content;
	}

});

app.seriesResolve = {
	series: function(SerieService){
		return SerieService.getAll().then(function(data){
			return data;
		},function(error){
			console.log(error)
		})
	}
};