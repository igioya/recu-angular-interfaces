'use strict';

// Declare app level module which depends on views, and components
let app = angular.module('myApp', [
  'ngRoute',
]);


app.config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');

  $routeProvider
  .when('/series', {
    templateUrl: 'partials/series.html',
    controller: 'SeriesCtrl as ctrl',
    resolve: app.seriesResolve
  })

  $routeProvider.otherwise({redirectTo: '/series'});
}]);
